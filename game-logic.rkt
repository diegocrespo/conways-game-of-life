#lang typed/racket

#| 
This file does all the heavy lifting in creating the grid, creating the enxt representation of the grid
Defining what the next grid will be, getting the neighbors of a point in the grid to determine whether it's alive or dead etc. There is no gui logic for rendering in this file. Look at the main.rkt file
|#

(provide (all-defined-out))

;; Constants
(: test-grid (Listof (List Integer Integer Integer Integer)))
(define test-grid '((1 1 1 0) (0 1 0 0) (1 1 1 1) (1 1 0 1)))

(: ROW Integer)
(define ROW 10)

(: COLUMNS Integer)
(define COLUMNS 10)

;; 2 dimensional list
(define-type Grid (Listof (Listof Integer)))

(: make-grid (-> Integer Integer Grid))
(define (make-grid rows columns)
  (for/list : (Listof (Listof Integer)) ([i rows])
    (for/list : (Listof Integer) ([j columns])
      (random 0 2))))

;; Takes an x y coord and returns up to 8 neighboring points in a list of lists
(: find-neighbors (-> Integer Integer Integer Integer (Listof (List Integer Integer))))
(define (find-neighbors row columns x y)
  (cond
    [(and (zero? x) (zero? y)) (list
                                (list x  (+ y 1))
                                (list (+ x 1) (+ y 1))
                                (list (+ x 1) y))] ; Top Left Corner 0,0

    [(and (> x 0) (< x (- row 1)) (zero? y)) (list
                                              (list (- x 1) y)
                                              (list (- x 1) (+ y 1))
                                              (list x (+ y 1))
                                              (list (+ x 1) (+ y 1))
                                              (list (+ x 1) y) )] ; Left Edge
    [(and (= x (- row 1)) (zero? y)) (list
                                      (list (- x 1) y)
                                      (list (- x 1) (+ y 1))
                                      (list x (+ y 1)) )] ; Bottom Left Corner

    [(and (zero? x) (> y 0) (< y (- columns 1))) (list
                                                  (list x (+ y 1))
                                                  (list (+ x 1) (+ y 1))
                                                  (list (+ x 1) y)
                                                  (list (+ x 1) (- y 1))
                                                  (list x (- y 1)))] ; Top Edge

    [(and (= y (- columns 1)) (zero? x)) (list
                                          (list (+ x 1) y)
                                          (list (+ x 1) (- y 1))
                                          (list x (- y 1)))] ; Top Right Corner

    [(and (> x 0) (< x (- row 1)) (= y (- columns 1))) (list
                                                        (list (- x 1) y)
                                                        (list (+ x 1) y)
                                                        (list (+ x 1) (- y 1))
                                                        (list x (- y 1))
                                                        (list (- x 1) (- y 1)))] ; Right Edge
    [(and (= x (- row 1)) (= y (- columns 1))) (list
                                                (list (- x 1) y)
                                                (list x (- y 1))
                                                (list (- x 1) (- y 1)))] ; Bottom Right Corner

    [(and (= x (- row 1)) (> y 0) (< y (- columns 1))) (list
                                                        (list (- x 1) y)
                                                        (list (- x 1) (+ y 1))
                                                        (list x (+ y 1))
                                                        (list x (- y 1))
                                                        (list (- x 1) (- y 1)))] ; Bottom Edge
    [else (list
           (list (- x 1) y)
           (list (- x 1) (+ y 1))
           (list x (+ y 1))
           (list (+ x 1) (+ y 1))
           (list (+ x 1) y)
           (list (+ x 1) (- y 1))
           (list x (- y 1))
           (list (- x 1) (- y 1)))]))  ; Center of grid with 8 neighbors

(: get-cell (-> Grid Integer Integer Integer)) 
(define (get-cell grid x y)
  (list-ref (list-ref  grid x) y))

(: sum-neighbors (-> Grid (Listof (List Integer Integer)) Integer))
(define (sum-neighbors grid neighbors)
  (cond
    [(null? neighbors) 0]
    [else (+ (get-cell grid (caar neighbors) (cadar neighbors)) (sum-neighbors grid (cdr neighbors)))]))

(: next-grid (-> Grid Integer Integer Grid))
(define (next-grid grid row columns)
  (for/list ([x row])
    (for/list : (Listof Integer) ([y columns])
      (let* ([cell (get-cell grid x y)] [neighbors (find-neighbors row columns x y)] [sum (sum-neighbors grid neighbors)])
        (cond
          [(and (zero? cell) (= sum 3)) 1]
          [(and (not (zero? cell)) (or  (= sum 3) (= sum 2))) 1]
          [else 0])))))
