I am by no means a Racket expert, but hopefully this code will be useful for learning more about gui programming in Racket, Typed/Racket, and Conways Game of life. There is an untyped version of the same program in the untyped-version folder. This could be used as practice for adding type annotations? This is not the version of the Game of Life that wraps around the array when finding neighbors of cells. Though maybe that could be a good excercise in modifying the find-neighbors function in the game-logic.rkt file

![Conways Game Of Life Example Gif](conways_gif.gif "Example of program running")

To run the program exectute `racket main.rkt`
