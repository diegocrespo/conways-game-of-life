I am by no means a Racket expert, but hopefully this code will be useful for learning more about gui programming in Racket, Typed/Racket, and Conways Game of life.

![Conways Game Of Life Example Gif](conways_gif.gif "Example of program running")

To run the program exectute `racket main.rkt`
